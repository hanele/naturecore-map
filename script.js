function showOverlay(element) {
  console.log(element);
  let overlay = document.getElementById(`${element.id}-ol`);
  overlay.style.display = "block";
}
function closeOverlay(element) {
  console.log(element);
  let parent = element.parentNode;
  parent.style.display = "none";
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function fade() {
  let regular = document.getElementById("naturecore-regular");
  let green = document.getElementById("naturecore-green");
  while (true) {
    green.style.opacity = 0;
    regular.style.opacity = 1;
    await sleep(5000);
    green.style.opacity = 1;
    regular.style.opacity = 0;
    await sleep(2400);
  }
}
